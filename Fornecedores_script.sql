USE [PROTHEUS_TESTE]
GO
/****** Object:  Table [dbo].[SA3010]    Script Date: 16/03/2021 13:01:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SA3010](
	[A3_FILIAL] [varchar](4) NOT NULL,
	[A3_COD] [varchar](6) NOT NULL,
	[A3_NOME] [varchar](40) NOT NULL,
	[A3_NREDUZ] [varchar](25) NOT NULL,
	[A3_END] [varchar](40) NOT NULL,
	[A3_BAIRRO] [varchar](20) NOT NULL,
	[A3_MUN] [varchar](15) NOT NULL,
	[A3_EST] [varchar](2) NOT NULL,
	[A3_CEP] [varchar](8) NOT NULL,
	[A3_DDDTEL] [varchar](3) NOT NULL,
	[A3_MSBLQL] [varchar](1) NOT NULL,
	[A3_TEL] [varchar](15) NOT NULL,
	[A3_FAX] [varchar](15) NOT NULL,
	[A3_TELEX] [varchar](10) NOT NULL,
	[A3_TIPO] [varchar](1) NOT NULL,
	[A3_CGC] [varchar](14) NOT NULL,
	[A3_INSCR] [varchar](18) NOT NULL,
	[A3_INSCRM] [varchar](18) NOT NULL,
	[A3_EMAIL] [varchar](80) NOT NULL,
	[A3_HPAGE] [varchar](30) NOT NULL,
	[A3_CODUSR] [varchar](6) NOT NULL,
	[A3_SUPER] [varchar](6) NOT NULL,
	[A3_GEREN] [varchar](6) NOT NULL,
	[A3_FORNECE] [varchar](6) NOT NULL,
	[A3_LOJA] [varchar](2) NOT NULL,
	[A3_GERASE2] [varchar](1) NOT NULL,
	[A3_BCO1] [varchar](3) NOT NULL,
	[A3_REGIAO] [varchar](3) NOT NULL,
	[A3_COMIS] [float] NOT NULL,
	[A3_ALEMISS] [float] NOT NULL,
	[A3_ALBAIXA] [float] NOT NULL,
	[A3_ICM] [varchar](1) NOT NULL,
	[A3_ICMSRET] [varchar](1) NOT NULL,
	[A3_UNIDAD] [varchar](6) NOT NULL,
	[A3_ISS] [varchar](1) NOT NULL,
	[A3_IPI] [varchar](1) NOT NULL,
	[A3_REGSLA] [varchar](6) NOT NULL,
	[A3_QTCONTA] [float] NOT NULL,
	[A3_GRPREP] [varchar](6) NOT NULL,
	[A3_FRETE] [varchar](1) NOT NULL,
	[A3_ACREFIN] [varchar](1) NOT NULL,
	[A3_DIA] [float] NOT NULL,
	[A3_DDD] [varchar](1) NOT NULL,
	[A3_CARGO] [varchar](6) NOT NULL,
	[A3_PERDESC] [float] NOT NULL,
	[A3_TIPSUP] [varchar](3) NOT NULL,
	[A3_DIARESE] [float] NOT NULL,
	[A3_SENHA] [varchar](6) NOT NULL,
	[A3_PEDINI] [varchar](6) NOT NULL,
	[A3_PEDFIM] [varchar](6) NOT NULL,
	[A3_CLIINI] [varchar](6) NOT NULL,
	[A3_CLIFIM] [varchar](6) NOT NULL,
	[A3_PROXPED] [varchar](6) NOT NULL,
	[A3_PROXCLI] [varchar](6) NOT NULL,
	[A3_SINCTAF] [varchar](1) NOT NULL,
	[A3_SINCAGE] [varchar](1) NOT NULL,
	[A3_FAT_RH] [varchar](2) NOT NULL,
	[A3_GRUPSAN] [varchar](6) NOT NULL,
	[A3_SINCCON] [varchar](1) NOT NULL,
	[A3_PERAGE] [varchar](1) NOT NULL,
	[A3_DEPEND] [varchar](2) NOT NULL,
	[A3_PEN_ALI] [float] NOT NULL,
	[A3_PERTAF] [varchar](1) NOT NULL,
	[A3_TIMEMIN] [varchar](4) NOT NULL,
	[A3_TIPVEND] [varchar](1) NOT NULL,
	[A3_USUCORP] [varchar](30) NOT NULL,
	[A3_URLEXG] [varchar](50) NOT NULL,
	[A3_NUMRA] [varchar](6) NOT NULL,
	[A3_PAIS] [varchar](3) NOT NULL,
	[A3_DDI] [varchar](6) NOT NULL,
	[A3_HABSINC] [varchar](1) NOT NULL,
	[A3_CEL] [varchar](15) NOT NULL,
	[A3_ADMISS] [varchar](8) NOT NULL,
	[A3_NVLSTR] [varchar](30) NOT NULL,
	[A3_NIVEL] [float] NOT NULL,
	[A3_LANEXG] [varchar](5) NOT NULL,
	[A3_PISCOF] [varchar](1) NOT NULL,
	[A3_BIAGEND] [varchar](1) NOT NULL,
	[A3_BICONT] [varchar](1) NOT NULL,
	[A3_BITAREF] [varchar](1) NOT NULL,
	[A3_EMACORP] [varchar](100) NOT NULL,
	[A3_DTUMOV] [varchar](8) NOT NULL,
	[A3_CODISS] [varchar](6) NOT NULL,
	[A3_HREXPO] [varchar](8) NOT NULL,
	[A3_HRUMOV] [varchar](5) NOT NULL,
	[A3_FILFUN] [varchar](4) NOT NULL,
	[A3_BASEIR] [varchar](1) NOT NULL,
	[A3_MSEXP] [varchar](8) NOT NULL,
	[A3_MODTRF] [varchar](1) NOT NULL,
	[A3_SNAEXG] [varchar](100) NOT NULL,
	[A3_USERLGA] [varchar](17) NOT NULL,
	[A3_USERLGI] [varchar](17) NOT NULL,
	[A3_HAND] [varchar](1) NOT NULL,
	[D_E_L_E_T_] [varchar](1) NOT NULL,
	[R_E_C_N_O_] [bigint] NOT NULL,
	[R_E_C_D_E_L_] [bigint] NOT NULL,
 CONSTRAINT [SA3010_PK] PRIMARY KEY CLUSTERED 
(
	[R_E_C_N_O_] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_FILIAL_DF]  DEFAULT ('    ') FOR [A3_FILIAL]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_COD_DF]  DEFAULT ('      ') FOR [A3_COD]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_NOME_DF]  DEFAULT ('                                        ') FOR [A3_NOME]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_NREDUZ_DF]  DEFAULT ('                         ') FOR [A3_NREDUZ]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_END_DF]  DEFAULT ('                                        ') FOR [A3_END]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_BAIRRO_DF]  DEFAULT ('                    ') FOR [A3_BAIRRO]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_MUN_DF]  DEFAULT ('               ') FOR [A3_MUN]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_EST_DF]  DEFAULT ('  ') FOR [A3_EST]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_CEP_DF]  DEFAULT ('        ') FOR [A3_CEP]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_DDDTEL_DF]  DEFAULT ('   ') FOR [A3_DDDTEL]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_MSBLQL_DF]  DEFAULT (' ') FOR [A3_MSBLQL]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_TEL_DF]  DEFAULT ('               ') FOR [A3_TEL]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_FAX_DF]  DEFAULT ('               ') FOR [A3_FAX]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_TELEX_DF]  DEFAULT ('          ') FOR [A3_TELEX]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_TIPO_DF]  DEFAULT (' ') FOR [A3_TIPO]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_CGC_DF]  DEFAULT ('              ') FOR [A3_CGC]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_INSCR_DF]  DEFAULT ('                  ') FOR [A3_INSCR]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_INSCRM_DF]  DEFAULT ('                  ') FOR [A3_INSCRM]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_EMAIL_DF]  DEFAULT ('                                                                                ') FOR [A3_EMAIL]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_HPAGE_DF]  DEFAULT ('                              ') FOR [A3_HPAGE]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_CODUSR_DF]  DEFAULT ('      ') FOR [A3_CODUSR]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_SUPER_DF]  DEFAULT ('      ') FOR [A3_SUPER]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_GEREN_DF]  DEFAULT ('      ') FOR [A3_GEREN]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_FORNECE_DF]  DEFAULT ('      ') FOR [A3_FORNECE]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_LOJA_DF]  DEFAULT ('  ') FOR [A3_LOJA]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_GERASE2_DF]  DEFAULT (' ') FOR [A3_GERASE2]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_BCO1_DF]  DEFAULT ('   ') FOR [A3_BCO1]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_REGIAO_DF]  DEFAULT ('   ') FOR [A3_REGIAO]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_COMIS_DF]  DEFAULT ((0)) FOR [A3_COMIS]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_ALEMISS_DF]  DEFAULT ((0)) FOR [A3_ALEMISS]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_ALBAIXA_DF]  DEFAULT ((0)) FOR [A3_ALBAIXA]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_ICM_DF]  DEFAULT (' ') FOR [A3_ICM]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_ICMSRET_DF]  DEFAULT (' ') FOR [A3_ICMSRET]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_UNIDAD_DF]  DEFAULT ('      ') FOR [A3_UNIDAD]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_ISS_DF]  DEFAULT (' ') FOR [A3_ISS]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_IPI_DF]  DEFAULT (' ') FOR [A3_IPI]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_REGSLA_DF]  DEFAULT ('      ') FOR [A3_REGSLA]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_QTCONTA_DF]  DEFAULT ((0)) FOR [A3_QTCONTA]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_GRPREP_DF]  DEFAULT ('      ') FOR [A3_GRPREP]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_FRETE_DF]  DEFAULT (' ') FOR [A3_FRETE]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_ACREFIN_DF]  DEFAULT (' ') FOR [A3_ACREFIN]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_DIA_DF]  DEFAULT ((0)) FOR [A3_DIA]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_DDD_DF]  DEFAULT (' ') FOR [A3_DDD]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_CARGO_DF]  DEFAULT ('      ') FOR [A3_CARGO]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_PERDESC_DF]  DEFAULT ((0)) FOR [A3_PERDESC]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_TIPSUP_DF]  DEFAULT ('   ') FOR [A3_TIPSUP]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_DIARESE_DF]  DEFAULT ((0)) FOR [A3_DIARESE]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_SENHA_DF]  DEFAULT ('      ') FOR [A3_SENHA]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_PEDINI_DF]  DEFAULT ('      ') FOR [A3_PEDINI]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_PEDFIM_DF]  DEFAULT ('      ') FOR [A3_PEDFIM]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_CLIINI_DF]  DEFAULT ('      ') FOR [A3_CLIINI]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_CLIFIM_DF]  DEFAULT ('      ') FOR [A3_CLIFIM]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_PROXPED_DF]  DEFAULT ('      ') FOR [A3_PROXPED]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_PROXCLI_DF]  DEFAULT ('      ') FOR [A3_PROXCLI]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_SINCTAF_DF]  DEFAULT (' ') FOR [A3_SINCTAF]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_SINCAGE_DF]  DEFAULT (' ') FOR [A3_SINCAGE]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_FAT_RH_DF]  DEFAULT ('  ') FOR [A3_FAT_RH]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_GRUPSAN_DF]  DEFAULT ('      ') FOR [A3_GRUPSAN]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_SINCCON_DF]  DEFAULT (' ') FOR [A3_SINCCON]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_PERAGE_DF]  DEFAULT (' ') FOR [A3_PERAGE]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_DEPEND_DF]  DEFAULT ('  ') FOR [A3_DEPEND]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_PEN_ALI_DF]  DEFAULT ((0)) FOR [A3_PEN_ALI]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_PERTAF_DF]  DEFAULT (' ') FOR [A3_PERTAF]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_TIMEMIN_DF]  DEFAULT ('    ') FOR [A3_TIMEMIN]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_TIPVEND_DF]  DEFAULT (' ') FOR [A3_TIPVEND]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_USUCORP_DF]  DEFAULT ('                              ') FOR [A3_USUCORP]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_URLEXG_DF]  DEFAULT ('                                                  ') FOR [A3_URLEXG]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_NUMRA_DF]  DEFAULT ('      ') FOR [A3_NUMRA]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_PAIS_DF]  DEFAULT ('   ') FOR [A3_PAIS]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_DDI_DF]  DEFAULT ('      ') FOR [A3_DDI]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_HABSINC_DF]  DEFAULT (' ') FOR [A3_HABSINC]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_CEL_DF]  DEFAULT ('               ') FOR [A3_CEL]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_ADMISS_DF]  DEFAULT ('        ') FOR [A3_ADMISS]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_NVLSTR_DF]  DEFAULT ('                              ') FOR [A3_NVLSTR]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_NIVEL_DF]  DEFAULT ((0)) FOR [A3_NIVEL]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_LANEXG_DF]  DEFAULT ('     ') FOR [A3_LANEXG]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_PISCOF_DF]  DEFAULT (' ') FOR [A3_PISCOF]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_BIAGEND_DF]  DEFAULT (' ') FOR [A3_BIAGEND]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_BICONT_DF]  DEFAULT (' ') FOR [A3_BICONT]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_BITAREF_DF]  DEFAULT (' ') FOR [A3_BITAREF]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_EMACORP_DF]  DEFAULT ('                                                                                                    ') FOR [A3_EMACORP]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_DTUMOV_DF]  DEFAULT ('        ') FOR [A3_DTUMOV]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_CODISS_DF]  DEFAULT ('      ') FOR [A3_CODISS]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_HREXPO_DF]  DEFAULT ('        ') FOR [A3_HREXPO]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_HRUMOV_DF]  DEFAULT ('     ') FOR [A3_HRUMOV]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_FILFUN_DF]  DEFAULT ('    ') FOR [A3_FILFUN]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_BASEIR_DF]  DEFAULT (' ') FOR [A3_BASEIR]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_MSEXP_DF]  DEFAULT ('        ') FOR [A3_MSEXP]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_MODTRF_DF]  DEFAULT (' ') FOR [A3_MODTRF]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_SNAEXG_DF]  DEFAULT ('                                                                                                    ') FOR [A3_SNAEXG]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_USERLGA_DF]  DEFAULT ('                 ') FOR [A3_USERLGA]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_USERLGI_DF]  DEFAULT ('                 ') FOR [A3_USERLGI]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_A3_HAND_DF]  DEFAULT (' ') FOR [A3_HAND]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_D_E_L_E_T__DF]  DEFAULT (' ') FOR [D_E_L_E_T_]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_R_E_C_N_O__DF]  DEFAULT ((0)) FOR [R_E_C_N_O_]
GO
ALTER TABLE [dbo].[SA3010] ADD  CONSTRAINT [SA3010_R_E_C_D_E_L__DF]  DEFAULT ((0)) FOR [R_E_C_D_E_L_]
GO
