import React from 'react'

import { NavigationContainer, useNavigation } from '@react-navigation/native';
import { createStackNavigator, HeaderBackButton } from '@react-navigation/stack';


import IconSync from 'react-native-vector-icons/MaterialIcons';
import IconAllClt from 'react-native-vector-icons/MaterialCommunityIcons';
import IconFormClt from 'react-native-vector-icons/AntDesign';
import IconMore from 'react-native-vector-icons/Foundation';
import IconSearch from 'react-native-vector-icons/Octicons';

import { View, Image, Text, StyleSheet, Button } from "react-native";

//Size Matters Calcs
import { Alert, Dimensions, TouchableOpacity } from 'react-native';
const { width, height } = Dimensions.get('window');
//Guideline sizes are based on standard ~5" screen mobile device
const guidelineBaseWidth = 350;
const guidelineBaseHeight = 680;
const scale = size => width / guidelineBaseWidth * size;
const verticalScale = size => height / guidelineBaseHeight * size;
const moderateScale = (size, factor = 0.5) => size + ( scale(size) - size ) * factor;
export { scale, verticalScale, moderateScale };

import { stylesStack } from './stylesStack';

import Login from '../src/pages/Login';
import Home from '../src/pages/Home';
import Sync from '../src/pages/Sync';
import ListarCadastrar from '../src/pages/ListarCadastrar';
import ListarClientes from '../src/pages/ListarCadastrar/ListarClientes';
import Cadastrar from '../src/pages/ListarCadastrar/Cadastrar';

const Stack = createStackNavigator();

onDeleteBTN = () => {
  alert(' Heloo333');
}
 
onDeleteBTN2 = () => {
  alert(' Heloo123');
}

//const navigation = useNavigation(); 

 
export default function App( ) {

 
  return (
 
    <NavigationContainer>

      <Stack.Navigator initialRouteName="Login">
        
        <Stack.Screen 
          name="Login" 
          component={Login}

          options={({ navigation }) => ({
            title: 'FORÇA DE VENDAS',
            headerStyle: {
              backgroundColor: '#fff',
              //opacity: 0.8,
              height: moderateScale(50, 1),
            },
            headerTitleStyle: {
              fontFamily: 'Montserrat-SemiBold',
              fontSize:  moderateScale(20, 1),
              alignSelf: 'center',
              
            },            
            headerTintColor: '#000',   
            // Removendo Back Button.
            //
            headerLeft: null,
          })}
            
        />

        <Stack.Screen 
          name="Home" 
          component={Home}
          options={({ navigation }) => ({
            title: null, //'BEM-VINDO',
            headerStyle: {
              backgroundColor: '#fff',
              height: moderateScale(50, 1),
              elevation: moderateScale(5),     
            },
            headerTitleStyle: {
              fontFamily: 'Montserrat-SemiBold',
              fontSize:  moderateScale(20, 1),
              alignSelf: 'center',
            },            
            headerTintColor: '#000',
            //Removendo Back Button
            //headerLeft: null,
            headerLeft: (props) =>
              <View style={stylesStack.top}>
                <Image style={stylesStack.logoCli}
                  source={require('../src/pages/assets/logos/logo.png')}
                />
              </View>,         

            headerRight:  (props) => 
            <View style={stylesStack.top}>
               
              <IconAllClt name="account-search-outline" style={stylesStack.topFstBtn} size={moderateScale(30)} onPress={(onDeleteBTN)}/>
              <IconFormClt name="form" style={stylesStack.topFstBtn} size={moderateScale(30)} onPress={(onDeleteBTN)}/>
              <IconSync name="sync-alt" style={stylesStack.topScdBtn} size={moderateScale(30)} onPress={(onDeleteBTN2)}/>
               
            </View>,
          })}
        />


        <Stack.Screen 
          name="Sync" 
          component={Sync}
          options={({ navigation }) => ({
            title: null, //Sincronismo
            headerStyle: {
              backgroundColor: '#fff',
              height: moderateScale(50, 1),
              elevation: moderateScale(5),    
            },
            headerTitleStyle: {
              fontFamily: 'Montserrat-SemiBold',
              fontSize:  moderateScale(20, 1),
              //alignSelf: 'center',
            },            
            headerTintColor: '#000',
            // Removendo Back Button
            //headerLeft: null,
            headerLeft: (props) =>
              <View style={stylesStack.top}>
                <Image style={stylesStack.logoCli}
                  source={require('../src/pages/assets/logos/logo.png')}
                />
              </View>       
          })}
        />


        <Stack.Screen 
          name="ListarCadastrar"
          component={ListarCadastrar}
          options={({ navigation }) => ({
            title: null, //Tela inicial
            headerStyle: {  
             backgroundColor: '#fff',
             height: moderateScale(50, 1),
             elevation: moderateScale(5),    
           },
           headerTitleStyle: {
             fontFamily: 'Montserrat-SemiBold',
             fontSize:  moderateScale(20, 1),
           },            
           headerTintColor: '#000',   

          headerRight:  (props) => 
            <View style={stylesStack.top}>
              <IconFormClt name="home" style={stylesStack.topFstBtn} size={moderateScale(30)} onPress = {() => navigation.navigate('Home')} />
              <IconAllClt name="account-search-outline" style={stylesStack.topFstBtn} size={moderateScale(30)} onPress = {() => navigation.navigate('Sync')} />
              <IconFormClt name="form" style={stylesStack.topFstBtn} size={moderateScale(30)} onPress={(onDeleteBTN)}/>
              <IconSync name="sync-alt" style={stylesStack.topScdBtn} size={moderateScale(30)} onPress={(onDeleteBTN2)}/>
              
            </View>,           
 
          headerLeft: (props) =>
            <View style={stylesStack.top}>
              <Image style={stylesStack.logoCli}
                source={require('../src/pages/assets/logos/logo.png')}
              />
            </View>
            // headerBackImage: () => 
            //   <BackIcon size={null} style={stylesStack.bkBtn} name="arrow-back-ios"/>,
            // headerLeft: (props) => (
            //   <HeaderBackButton
            //     {...props}
            //     onPress={onDeleteBTN}
            //   />
            // ),

          })} 
        />


        <Stack.Screen 
          name="ListarClientes"
          component={ListarClientes}
          options={({ navigation }) => ({
            title: null, //Tela inicial
            headerStyle: {
              backgroundColor: '#fff',
              height: moderateScale(50, 1),
              elevation: moderateScale(5),    
            },
            headerTitleStyle: {
              fontFamily: 'Montserrat-SemiBold',
              fontSize:  moderateScale(20, 1),
              //alignSelf: 'center',
            },            
            headerTintColor: '#000',     

            headerRight:  (props) => 
              <View style={stylesStack.top}>
                
                <IconSearch name="search" style={stylesStack.topFstBtn} size={moderateScale(30)} onPress={(onDeleteBTN)} onPress={() => navigation.push('Home')}  />
                <IconMore name="indent-more" style={stylesStack.topScdBtn} size={moderateScale(30)} onPress={(onDeleteBTN2)}/>
               
              </View>,
            // Removendo Back Button
            //headerLeft: null,
            headerLeft: (props) =>
              <View style={stylesStack.top}>
                <Image style={stylesStack.logoCli}
                  source={require('../src/pages/assets/logos/logo.png')}
                />
              </View>
          })}
        />


        <Stack.Screen 
          name="Cadastrar"
          component={Cadastrar}
          options={({ navigation }) => ({
            title: null, //Tela inicial
            headerStyle: {
              backgroundColor: '#fff',
              height: moderateScale(50, 1),
              elevation: moderateScale(5),    
            },
            headerTitleStyle: {
              fontFamily: 'Montserrat-SemiBold',
              fontSize:  moderateScale(20, 1),
              //alignSelf: 'center',
            },            
            headerTintColor: '#000',     

            headerRight:  (props) => 
              <View style={stylesStack.top}>
                <IconAllClt name="account-search-outline" style={stylesStack.topFstBtn} size={moderateScale(30)} onPress={() => navigation.push('Home')} />
                <IconFormClt name="form" style={stylesStack.topFstBtn} size={moderateScale(30)} onPress={(onDeleteBTN)}/>
                <IconSync name="sync-alt" style={stylesStack.topScdBtn} size={moderateScale(30)} onPress={(onDeleteBTN2)}/>
               
              </View>,
            // Removendo Back Button
            //headerLeft: null,
            headerLeft: (props) =>
              <View style={stylesStack.top}>
                <Image style={stylesStack.logoCli}
                  source={require('../src/pages/assets/logos/logo.png')}
                />
              </View>
          })}
        />

      </Stack.Navigator>
    </NavigationContainer>
  );
}
 
