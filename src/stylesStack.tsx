import { StyleSheet } from "react-native";
//Size Matters Calcs
import { Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
//Guideline sizes are based on standard ~5" screen mobile device
const guidelineBaseWidth = 350;
const guidelineBaseHeight = 680;
const scale = size => width / guidelineBaseWidth * size;
const verticalScale = size => height / guidelineBaseHeight * size;
const moderateScale = (size, factor = 0.5) => size + ( scale(size) - size ) * factor;
export {scale, verticalScale, moderateScale};


import styled from 'styled-components';
import LinearGradient from 'react-native-linear-gradient';
 
export const ContainerGradient = styled(LinearGradient).attrs({
  colors: ['#fff' ,'#F8F8FF'], //F5F5F5 F8F8FF or //F0F8FF
  start: { x: 0, y: 0 },
  end: { x: 1, y: 1 },
})`
  flex: 1;
  justifyContent: center;
  alignItems: center;
`;

export const stylesStack = StyleSheet.create({
  top: {
    flexWrap: 'wrap', 
    flexDirection:'row',

    //alignItems: 'flex-end',
  },
  topFstBtn: {
    paddingRight: moderateScale(36, 1.4),
    color: '#000',
  },
  topScdBtn: {
    paddingRight: moderateScale(10, 1.4),
    color: '#000',
  },
  bkBtn: {
    paddingRight: moderateScale(10, 1),
    color: '#000',
  },
  
  logoCli: {
    width: moderateScale(68, 1),
    height: moderateScale(40, 1 ),
    marginLeft: moderateScale(8, 1),

  },
  // syncHome: {
  //   top: moderateScale(22, 1),
  // },
  // btnToIconA: {
  //   paddingLeft: moderateScale(10, 1),
  //   width: moderateScale(315, 1),
  //   paddingTop: moderateScale(1, 5),
  //   paddingBottom: moderateScale(1, 5),    
  //   backgroundColor: '#7FB3D5', //#5DADE2
  //   borderRadius: moderateScale(4),
  //   shadowColor: "#fff",
  //   shadowOffset: {
  //     width: moderateScale(0),
  //     height: moderateScale(2),
  //   },
  //   shadowOpacity: moderateScale(0.25),
  //   shadowRadius: moderateScale(3.84),
  //   elevation: moderateScale(8),    
  // },
  // botaoText: {
  //   paddingLeft: moderateScale(97),   
  //   bottom: moderateScale(40),
  //   fontSize: moderateScale(20, 1),
  //   marginLeft: 'auto',
  //   marginRight: 'auto',
  //   color: '#fff',
  //   fontFamily: 'Raleway-SemiBold',
  // },

  // btnToIconB: {
  //   paddingLeft: moderateScale(10, 1),
  //   width: moderateScale(315, 1),
  //   paddingTop: moderateScale(1, 5),
  //   paddingBottom: moderateScale(1, 5),    
  //   backgroundColor: '#7FB3D5', // #263f93 #7FB3D5
  //   borderRadius: moderateScale(4),
  //   shadowColor: "#fff",
  //   shadowOffset: {
  //     width: moderateScale(0),
  //     height: moderateScale(2),
  //   },
  //   shadowOpacity: moderateScale(0.25),
  //   shadowRadius: moderateScale(3.84),
  //   elevation: moderateScale(8),    
  // },
  // botaoTextLogOut: {
  //   paddingLeft: moderateScale(97),   
  //   bottom: moderateScale(40),
  //   fontSize: moderateScale(20, 1),
  //   marginLeft: 'auto',
  //   marginRight: 'auto',
  //   color: '#dcdcdc',
  //   fontFamily: 'Raleway-SemiBold',
  // },

});
