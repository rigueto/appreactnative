import React from 'react'
//import styled from 'styled-components'
import styled from 'styled-components';
import LinearGradient  from 'react-native-linear-gradient';
import { getStatusBarHeight } from 'react-native-status-bar-height';

export const Container = styled(LinearGradient).attrs({
  colors: ['#4169E1', '#ADD8E6'],
  start: { x: 0, y: 0 },
  end: { x: 1, y: 1 },
})`
  flex: 1;
  padding-top: ${30 + getStatusBarHeight(true)}px;
`;

export const Title = styled.Text`
  font-size: 32px;
  color: #FFF;
  font-weight: bold;
  padding: 0 20px;
`;

export const Form = styled.View`
  flex-direction: row;
  margin-top: 10px;
  padding: 0 20px;
`;

export const Input = styled.TextInput.attrs({
  placeholderTextColor: '#999',
})`
  flex: 1;
  padding: 12px 15px;
  border-radius: 4px;
  font-size: 16px;
  color: #333;
  background: #FFF;
  border: 2px solid ${props => (props.error ? '#FF7272' : '#FFf')};
`;

export const Submit = styled.TouchableOpacity`
  background: #000;
  margin-left: 10px;
  justify-content: center;
  border-radius: 4px;
  padding: 0 14px;
`;

export const List = styled.FlatList.attrs({
  contentContainerStyle: { paddingHorizontal: 20 },
  showsVerticalScrollIndicator: false,
})`
  margin-top: 20px;
`;




  // container: styled(LinearGradient).attrs({
  //   colors: ['#4169E1', '#ADD8E6'],
  //   start: { x: 0, y: 0 },
  //   end: { x: 1, y: 1 },
  // }),

  // logo: {
  //   width: 150,
  //   height: 150,
  //   borderRadius: 100
  // },
  // input: {
  //   marginTop: 10,
  //   padding: 10,
  //   width: 300,
  //   backgroundColor: '#fff',
  //   fontSize: 16,
  //   fontWeight: 'bold',
  //   borderRadius: 3
  // },
  // botao: {
  //   width: 300,
  //   height: 42,
  //   backgroundColor: '#3498db',
  //   marginTop: 10,
  //   borderRadius: 4,
  //   alignItems: 'center',
  //   justifyContent: 'center'
  // },
  // botaoText: {
  //   fontSize: 16,
  //   fontWeight: 'bold',
  //   color: '#fff'
  // }