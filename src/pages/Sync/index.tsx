import React, { useState } from 'react'

import { stylesSync } from './stylesSync';
import { Text, View, ScrollView, TouchableOpacity, TouchableHighlight } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import IconSync from 'react-native-vector-icons/MaterialIcons';
import IconPeoples from 'react-native-vector-icons/FontAwesome5';
import IconProducts from 'react-native-vector-icons/FontAwesome5';

//Size Matters Calcs
import { Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
//Guideline sizes are based on standard ~5" screen mobile device
const guidelineBaseWidth = 350;
const guidelineBaseHeight = 680;
const scale = size => width / guidelineBaseWidth * size;
const verticalScale = size => height / guidelineBaseHeight * size;
const moderateScale = (size, factor = 0.5) => size + ( scale(size) - size ) * factor;
export {scale, verticalScale, moderateScale};

import { ContainerGradient} from './stylesSync';
import LinearGradient from 'react-native-linear-gradient';

export default function Sync({ navigation }) {

  return (

    <ContainerGradient>
 
        <ScrollView>

        {/* Para horizontal {{x: 0, y: 0}} end={{x: 1, y: 0}} sem fica na vertical o gradient */}
        <TouchableOpacity style={stylesSync.paddingFstBtn} onPress={() => navigation.navigate('Sync')}>
          <LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']} 
            style={stylesSync.linearGradient}
          >
            <IconSync name="sync-alt"  
              size={moderateScale(80)} 
              color='#fff'
            /> 
            <Text style={stylesSync.buttonText}>
            SINCRONIZAR TUDO
            </Text>
          </LinearGradient>
        </TouchableOpacity>

        <TouchableOpacity style={stylesSync.paddings} onPress={() => navigation.navigate('ListarCadastrar')}>
          <LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']} style={stylesSync.linearGradient}>
            <IconPeoples name="people-arrows" 
              size={moderateScale(80)} 
              color='#fff'
            /> 
            <Text style={stylesSync.buttonText}>
            SINCRONIZAR CLIENTES
            </Text>
          </LinearGradient>
        </TouchableOpacity>
        

        <TouchableOpacity style={stylesSync.paddings} onPress={() => navigation.navigate('Login')}>
          <LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']} style={stylesSync.linearGradient}>
            <IconProducts name="boxes" 
              size={moderateScale(80)} 
              color='#fff'
            /> 
            <Text style={stylesSync.buttonText}>
              SINCRONIZAR PRODUTOS
            </Text>
          </LinearGradient>
        </TouchableOpacity>

 

        </ScrollView>
 
    </ContainerGradient>
  )
}
