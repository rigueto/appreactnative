import { StyleSheet } from "react-native";
//Size Matters Calcs
import { Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
//Guideline sizes are based on standard ~5" screen mobile device
const guidelineBaseWidth = 350;
const guidelineBaseHeight = 680;
const scale = size => width / guidelineBaseWidth * size;
const verticalScale = size => height / guidelineBaseHeight * size;
const moderateScale = (size, factor = 0.5) => size + ( scale(size) - size ) * factor;
export {scale, verticalScale, moderateScale};

import styled from 'styled-components';
import LinearGradient from 'react-native-linear-gradient';
 
export const ContainerGradient = styled(LinearGradient).attrs({
  colors: ['#fff' ,'#F5F5F5'], //F5F5F5 F8F8FF or //F0F8FF
  start: { x: 0, y: 0 },
  end: { x: 1, y: 1 },
})`
  flex: 1;
  justifyContent: center;
  alignItems:center;
`;

export const stylesLogout = StyleSheet.create({

  paddings: {
    paddingTop: moderateScale(8, 1),
    paddingBottom: moderateScale(8, 1),    
  },

  linearGradient: {
    //flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 5,
    //flexWrap: 'wrap', 
    flexDirection:'row',
    //display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    
  },
  buttonText: {
    fontSize: moderateScale(22),
    fontFamily: 'Gill Sans',
    textAlign: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    //bottom: 100,
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
  
  botaoTextModal: {
    fontSize: moderateScale(22),
    fontFamily: 'Gill Sans',
    textAlign: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    //bottom: 100,
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
  // paddingsModal: {
  //   marginTop: 30,
  //   marginBottom: 30,
  //   paddingBottom: 30,
  //   paddingTop: 30,
  // },
  container: {
    flex:1,
    justifyContent: 'center',
    alignItems:'center',
    backgroundColor: '#fff',
  },  
  box: {
    width: moderateScale(300),
    height: verticalScale(450),    
  },
  logoCli: {
    width: moderateScale(155),
    height: moderateScale(90),
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  minosLogo: {
    width: moderateScale(140),
    height: moderateScale(60),
    marginLeft: 'auto',
    marginRight: 'auto',
    bottom: moderateScale(-50)
  },  
  input: {
    marginTop: moderateScale(10),
    padding: scale(10),
    height: moderateScale(44),
    width: '100%',
    backgroundColor: '#f4f4f4',//'#E8E8E8', //'#F8F8FF',
    fontSize: moderateScale(16),
    color: '#333',
    borderRadius: moderateScale(4),
    //elevation: moderateScale(0.2),
  },
  loginBtn: {
    width: '100%',
    height: moderateScale(45),
    marginTop: moderateScale(10),
    backgroundColor: '#263f93', //#f38624 //#263f93
    borderRadius: moderateScale(4),
    alignItems: 'center',
    justifyContent: 'center',
    //elevation: moderateScale(8),   
  },
  botaoText: {
    fontSize: moderateScale(16),
    fontFamily: 'Montserrat-SemiBold',
    color: '#fff'
  },
  settingsLogout: {
    marginLeft: 'auto',//Para limitar o click somente em cima do settings.
    alignItems: 'flex-end',
    top: moderateScale(70, 1),
  },
  modal: {
    top: '30%',  
    position: 'absolute',
    backgroundColor: '#fff',
    width: '100%',
    borderRadius: 5,
    paddingLeft: 25,
    paddingRight: 25
  },
  indicator: {
    width: 50,
    height: 5,
    backgroundColor: '#ccc',
    borderRadius: 50,
    alignSelf: 'center',
    marginTop: 5,
  },
    tituloText: {
    marginTop: moderateScale(10),
    marginBottom: moderateScale(10),
    marginLeft: 'auto',
    marginRight: 'auto',
    fontSize: moderateScale(17),
    fontFamily: 'Montserrat-SemiBold',
    color: '#000',  
  },
  inputModal: {
    marginTop: moderateScale(10),
    padding: scale(10),
    width: '100%',
    backgroundColor: '#F8F8FF',
    fontSize: moderateScale(16),
    fontFamily: 'Montserrat-Light',
    color: '#333',
    borderRadius: 4,
  },  
  botaoTextModal: {
    fontSize: moderateScale(16),
    color: '#fff',
    fontFamily: 'Montserrat-SemiBold',
    elevation: 8,
  },  
    modalBtn: {
    width: '100%',
    height: moderateScale(42),
    padding: scale(15),
    marginTop: moderateScale(10),
    marginBottom: moderateScale(40),
    backgroundColor: '#263f93', //#f38624 //#263f93
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center'
  },
    modalContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    position: 'absolute',
  },  

  minosName: {
    top: '10%',
    fontFamily: 'Backyard',
    fontSize: moderateScale(32),
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  minosSoft: {
    top: '10%',
    fontFamily: 'Backyard',
    fontSize: moderateScale(12),
    marginLeft: 'auto',
    marginRight: 'auto',
    marginBottom: '8%',
  }
});
