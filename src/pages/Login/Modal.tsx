import React, { useState, useEffect } from 'react'
import { TextInput, View, Text, StyleSheet, TouchableOpacity, Animated, Dimensions } from 'react-native'
import { stylesLogout } from './stylesLogout';
//Size Matters Calcs
const { width, height } = Dimensions.get('window');
//Guideline sizes are based on standard ~5" screen mobile device
const guidelineBaseWidth = 350;
const guidelineBaseHeight = 680;
const scale = size => width / guidelineBaseWidth * size;
const verticalScale = size => height / guidelineBaseHeight * size;
const moderateScale = (size, factor = 0.5) => size + ( scale(size) - size ) * factor;
export {scale, verticalScale, moderateScale};

import LinearGradient from 'react-native-linear-gradient';

const Modal = ({ show, close }) => {
  const [state, setState] = useState({
    opacity: new Animated.Value(0),
    modalContainer: new Animated.Value(height),
    modal: new Animated.Value(height)
  })

  const openModal = () => {
    Animated.sequence([
      Animated.timing(state.modalContainer, { toValue: 0, duration: 100, useNativeDriver: true }),
      Animated.timing(state.opacity, { toValue: 1, duration: 300, useNativeDriver: true }),
      Animated.spring(state.modal, { toValue: 0, bounciness: 5, useNativeDriver: true }),
    ]).start()
  }

  const closeModal = () => {
    Animated.sequence([
      Animated.timing(state.modal, { toValue: height, duration: 250, useNativeDriver: true }),
      Animated.timing(state.opacity, { toValue: 0, duration: 300, useNativeDriver: true }),
      Animated.timing(state.modalContainer, { toValue: height, duration: 100, useNativeDriver: true })
    ]).start()
  }

  useEffect(() => {
    if(show){
      openModal()
    }else{
      closeModal()
    }
  }, [show])

  return( 
    <Animated.View 
      style={[stylesLogout.modalContainer, {
        opacity: state.opacity,
        transform: [
          { translateY: state.modalContainer }
        ]
      }]}
    >
      <Animated.View 
        style={[stylesLogout.modal, {
          transform: [
            { translateY: state.modal }
          ]
        }]}
      >
 
        <View style={stylesLogout.indicator} />

        <Text style={stylesLogout.tituloText}>IDENTIFIQUE SUA EMPRESA</Text>
        <TextInput
          style={stylesLogout.inputModal}
          placeholder='Código da Empresa'
          placeholderTextColor = '#999'
        />
          
        {/* <TouchableOpacity style={stylesLogout.modalBtn} onPress={close}>
          <Text style={stylesLogout.botaoTextModal}>SALVAR</Text>
        </TouchableOpacity> */}

        <TouchableOpacity style={stylesLogout.paddings} onPress={close}>
          <LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']} 
            paddingTop={moderateScale(10)} 
            paddingBottom={moderateScale(10)} 
            style={stylesLogout.linearGradient}
          >
          <Text style={stylesLogout.botaoTextModal}>
            SALVAR
          </Text>
        </LinearGradient>
      </TouchableOpacity>   

      </Animated.View>
    </Animated.View>
  )
}

export default Modal
