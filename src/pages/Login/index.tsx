import React, { useState } from 'react'
 
import Icon from 'react-native-vector-icons/FontAwesome';
//import IconLogin from 'react-native-vector-icons/MaterialCommunityIcons';
import Modal from './Modal'

import { stylesLogout } from './stylesLogout';
import { Text, View, Image, TextInput, TouchableOpacity } from 'react-native';
import { ContainerGradient } from './stylesLogout';
import LinearGradient from 'react-native-linear-gradient';

//Size Matters Calcs
import { Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
//Guideline sizes are based on standard ~5" screen mobile device
const guidelineBaseWidth = 350;
const guidelineBaseHeight = 680;
const scale = size => width / guidelineBaseWidth * size;
const verticalScale = size => height / guidelineBaseHeight * size;
const moderateScale = (size, factor = 0.5) => size + ( scale(size) - size ) * factor;
export {scale, verticalScale, moderateScale};

export default function Login({ navigation }) {

  const [modal, setModal] = useState(false)
 
  return (
 
    <ContainerGradient>
      <View style={stylesLogout.box}>
        {/* <Image style={stylesLogout.logoCli}
          source={require('../assets/logos/logo.png')}
        /> */}
        <View style={stylesLogout.paddings}>
          <Text style={stylesLogout.minosName}>minos</Text>
          <Text style={stylesLogout.minosSoft}>soFTwARE</Text>
        </View>
        
        <TextInput style={stylesLogout.input}
          placeholder='Digite seu email'
          autoCapitalize="none"
          placeholderTextColor="#708090"
          autoCorrect={false}          
        />

        <TextInput style={stylesLogout.input}
          secureTextEntry={true}
          placeholderTextColor="#708090"
          placeholder='Digite sua senha'
        />

        {/* <TouchableOpacity style={stylesLogout.loginBtn}
          onPress={() => navigation.navigate('Home')}
        >
          <TextInput style={stylesLogout.botaoText}>ENTRAR</TextInput>
        </TouchableOpacity> */}

      <TouchableOpacity style={stylesLogout.paddings} onPress={() => navigation.navigate('Home')}>
        <LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']} 
          style={stylesLogout.linearGradient}
          paddingTop={moderateScale(12)} 
          paddingBottom={moderateScale(12)}
        >
          {/* <IconLogin name="login-variant" 
            size={moderateScale(45)} 
            color='#fff'
          />  */}
             

          <Text style={stylesLogout.botaoText}>
            ENTRAR
          </Text>
        </LinearGradient>
      </TouchableOpacity>    

        {/* <Text style={stylesLogout.minosName}>minos</Text>
        <Text style={stylesLogout.minosSoft}>soFTwARE</Text> */}

        <TouchableOpacity style={stylesLogout.settingsLogout}  onPress={() => setModal(true)}>
          <Icon 
            name="gears" 
            size={moderateScale(50)} 
            color='#cfd0d2' 
          />
        </TouchableOpacity>

        <Modal 
          show={modal}
          close={() => setModal(false)}
        />
 
      </View>
      </ContainerGradient>
  )
}
