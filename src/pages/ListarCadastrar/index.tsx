import React, { useState } from 'react'
 
import Icon from 'react-native-vector-icons/FontAwesome';
 
import { Text, ScrollView, View, Image, TextInput, TouchableOpacity } from 'react-native';
import { ContainerGradient } from './stylesListarCadastrar';
import { stylesListarCadastrar } from './stylesListarCadastrar';
import LinearGradient from 'react-native-linear-gradient';

import IconAllClt from 'react-native-vector-icons/MaterialCommunityIcons';
import IconFormClt from 'react-native-vector-icons/AntDesign';
 
//Size Matters Calcs
import { Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
//Guideline sizes are based on standard ~5" screen mobile device
const guidelineBaseWidth = 350;
const guidelineBaseHeight = 680;
const scale = size => width / guidelineBaseWidth * size;
const verticalScale = size => height / guidelineBaseHeight * size;
const moderateScale = (size, factor = 0.5) => size + ( scale(size) - size ) * factor;
export {scale, verticalScale, moderateScale};

export default function ListarCadastrar ({ navigation }) {

 
  return (
 
    <ContainerGradient>
      <ScrollView>
 
        
        {/* Para horizontal {{x: 0, y: 0}} end={{x: 1, y: 0}} sem fica na vertical o gradient */}
        <TouchableOpacity style={stylesListarCadastrar.paddingFstBtn} onPress={() => navigation.navigate('ListarClientes')}>
          <LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']} 
            style={stylesListarCadastrar.linearGradient}
          >
            <IconAllClt name="account-search-outline"  
              size={moderateScale(80)} 
              color='#fff'
            /> 
            <Text style={stylesListarCadastrar.buttonText}>
            LISTAR TODOS
            </Text>
          </LinearGradient>
        </TouchableOpacity>

        <TouchableOpacity style={stylesListarCadastrar.paddings} onPress={() => navigation.navigate('Cadastrar')}>
          <LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']} style={stylesListarCadastrar.linearGradient}>
            <IconFormClt name="form" 
              size={moderateScale(80)} 
              color='#fff'
            /> 
            <Text style={stylesListarCadastrar.buttonText}>
            CADASTRAR CLIENTE
            </Text>
          </LinearGradient>
        </TouchableOpacity>
 
 
    
      </ScrollView>
    </ContainerGradient>
  )
}
