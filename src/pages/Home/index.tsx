import React, { useState } from 'react'

import { stylesHome } from './stylesHome';
import { Text, View, ScrollView, TouchableOpacity, Button, TouchableHighlight } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import IconPeople from 'react-native-vector-icons/MaterialIcons';
import IconCircle from 'react-native-vector-icons/MaterialCommunityIcons';
import IconShoppingcart from 'react-native-vector-icons/AntDesign';
import IconOcticons from 'react-native-vector-icons/Octicons';
import IconLogOut from 'react-native-vector-icons/Entypo';

//Size Matters Calcs
import { Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
//Guideline sizes are based on standard ~5" screen mobile device
const guidelineBaseWidth = 350;
const guidelineBaseHeight = 680;
const scale = size => width / guidelineBaseWidth * size;
const verticalScale = size => height / guidelineBaseHeight * size;
const moderateScale = (size, factor = 0.5) => size + ( scale(size) - size ) * factor;
export {scale, verticalScale, moderateScale};


import LinearGradient from 'react-native-linear-gradient';


import { ContainerGradient} from './stylesHome';

export default function Home({ navigation }) {

  return (

    <ContainerGradient>
      
        <ScrollView>

      {/* Para horizontal {{x: 0, y: 0}} end={{x: 1, y: 0}} sem fica na vertical o gradient */}
      <TouchableOpacity style={stylesHome.paddingFstBtn} onPress={() => navigation.navigate('Sync')}>
        <LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']} 
          style={stylesHome.linearGradient}
        >
          <IconCircle name="sync-circle" 
            size={moderateScale(80)} 
            color='#fff'
          /> 
          <Text style={stylesHome.buttonText}>
            SINCRONIZAÇÃO
          </Text>
        </LinearGradient>
      </TouchableOpacity>

      <TouchableOpacity style={stylesHome.paddings} onPress={() => navigation.navigate('ListarCadastrar')}>
        <LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']} style={stylesHome.linearGradient}>
          <IconPeople name="people" 
            size={moderateScale(80)} 
            color='#fff'
          /> 
          <Text style={stylesHome.buttonText}>
            CLIENTES
          </Text>
        </LinearGradient>
      </TouchableOpacity>
      

      <TouchableOpacity style={stylesHome.paddings} onPress={() => navigation.navigate('Login')}>
        <LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']} style={stylesHome.linearGradient}>
          <IconShoppingcart name="shoppingcart" 
            size={moderateScale(80)} 
            color='#fff'
          /> 
          <Text style={stylesHome.buttonText}>
            PEDIDOS
          </Text>
        </LinearGradient>
      </TouchableOpacity>

      <TouchableOpacity style={stylesHome.paddings} onPress={() => navigation.navigate('Login')}>
        <LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']} style={stylesHome.linearGradient}>
          <IconOcticons name="graph" 
            size={moderateScale(80)} 
            color='#fff'
          /> 
          <Text style={stylesHome.buttonText}>
            RELATÓRIOS
          </Text>
        </LinearGradient>
      </TouchableOpacity>            

      <TouchableOpacity style={stylesHome.paddings} onPress={() => navigation.navigate('Login')}> 
        <LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']} style={stylesHome.linearGradient}>
          <IconLogOut name="log-out" 
            size={moderateScale(80)} 
            color='#fff'
          /> 
          <Text style={stylesHome.buttonText}>
          SAIR
          </Text>
        </LinearGradient>
      </TouchableOpacity>

          {/* <TouchableOpacity
            style={stylesHome.syncHome}  
            onPress={() => navigation.navigate('Sync')}
          >
            <IconCircle name="sync-circle" 
              size={moderateScale(80)} 
              color='#fff'
              style={stylesHome.btnToIconA}
            /> 
          </TouchableOpacity>        
          <Text style={stylesHome.botaoText}>SINCRONIZAÇÃO</Text>


          <TouchableOpacity
            style={stylesHome.syncHome}  
            onPress={() => navigation.navigate('ListarCadastrar')}
          >
            <IconPeople name="people" 
              size={moderateScale(80)} 
              color='#fff'
              style={stylesHome.btnToIconA}
            />           
          </TouchableOpacity>        
          <Text style={stylesHome.botaoText}>CLIENTES</Text>


          <TouchableOpacity
            style={stylesHome.syncHome}  
            onPress={() => navigation.navigate('Login')}
          >
            <IconShoppingcart name="shoppingcart" 
              size={moderateScale(80)} 
              color='#fff'
              style={stylesHome.btnToIconA}
            /> 
          </TouchableOpacity>        
          <Text style={stylesHome.botaoText}>PEDIDOS</Text>


          <TouchableOpacity          
            style={stylesHome.syncHome}  
            onPress={() => navigation.navigate('Login')}
          >
            <IconOcticons name="graph" 
              size={moderateScale(80)} 
              color='#fff'
              style={stylesHome.btnToIconA}
            /> 
          </TouchableOpacity>        
          <Text style={stylesHome.botaoText}>RELATÓRIOS</Text>


          <TouchableOpacity          
            style={stylesHome.syncHome}  
            onPress={() => navigation.navigate('Login')}
          >
            <IconLogOut name="log-out" 
              size={moderateScale(80)} 
              color='#dcdcdc'
              style={stylesHome.btnToIconB}
            /> 
          </TouchableOpacity>        
          <Text style={stylesHome.botaoTextLogOut}>SAIR</Text> */}

  
        </ScrollView>
     
    </ContainerGradient>
  )
}
